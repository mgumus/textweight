//import liraries
import React, { Component } from 'react';


// create a component
class App extends Component {
    constructor(props){
        super(props)
        this.state = { wordItems: [], 
            data:`Birinci Söz
            Bismillah her hayrın başıdır. Biz dahi başta ona başlarız. Bil ey nefsim, şu mübarek kelime İslâm nişanı olduğu gibi bütün mevcudatın lisan-ı haliyle vird-i zebanıdır.
            Bismillah ne büyük tükenmez bir kuvvet, ne çok bitmez bir bereket olduğunu anlamak istersen şu temsilî hikâyeciğe bak, dinle. Şöyle ki:
            Bedevî Arap çöllerinde seyahat eden adama gerektir ki bir kabile reisinin ismini alsın ve himayesine girsin, tâ şakîlerin şerrinden kurtulup hâcatını tedarik edebilsin. Yoksa tek başıyla hadsiz düşman ve ihtiyacatına karşı perişan olacaktır.
            İşte böyle bir seyahat için iki adam sahraya çıkıp gidiyorlar. Onlardan birisi mütevazi idi, diğeri mağrur. Mütevazii, bir reisin ismini aldı. Mağrur, almadı. Alanı, her yerde selâmetle gezdi. Bir kātıu’t-tarîke rast gelse der: “Ben, filan reisin ismiyle gezerim.” Şakî def’olur, ilişemez. Bir çadıra girse o nam ile hürmet görür. Öteki mağrur, bütün seyahatinde öyle belalar çeker ki tarif edilmez. Daima titrer, daima dilencilik ederdi. Hem zelil hem rezil oldu.
            İşte ey mağrur nefsim, sen o seyyahsın. Şu dünya ise bir çöldür. Aczin ve fakrın hadsizdir. Düşmanın, hâcatın nihayetsizdir. Madem öyledir, şu sahranın Mâlik-i Ebedî’si ve Hâkim-i Ezelî’sinin ismini al. Tâ bütün kâinatın dilenciliğinden ve her hâdisatın karşısında titremeden kurtulasın.
            Evet, bu kelime öyle mübarek bir definedir ki senin nihayetsiz aczin ve fakrın, seni nihayetsiz kudrete, rahmete rabtedip Kadîr-i Rahîm’in dergâhında aczi, fakrı en makbul bir şefaatçi yapar.
            Evet, bu kelime ile hareket eden, o adama benzer ki askere kaydolur, devlet namına hareket eder. Hiçbir kimseden pervası kalmaz. Kanun namına, devlet namına der, her işi yapar, her şeye karşı dayanır.
            Başta demiştik: Bütün mevcudat, lisan-ı hal ile Bismillah der. Öyle mi?
            Evet, nasıl ki görsen, bir tek adam geldi, bütün şehir ahalisini cebren bir yere sevk etti ve cebren işlerde çalıştırdı. Yakînen bilirsin; o adam kendi namıyla, kendi kuvvetiyle hareket etmiyor. Belki o, bir askerdir, devlet namına hareket eder, bir padişah kuvvetine istinad eder.
            Öyle de her şey, Cenab-ı Hakk’ın namına hareket eder ki zerrecikler gibi tohumlar, çekirdekler başlarında koca ağaçları taşıyor, dağ gibi yükleri kaldırıyorlar.
            Demek her bir ağaç, Bismillah der. Hazine-i rahmet meyvelerinden ellerini dolduruyor, bizlere tablacılık ediyor.
            Her bir bostan, Bismillah der. Matbaha-i kudretten bir kazan olur ki çeşit çeşit, pek çok muhtelif leziz taamlar, içinde beraber pişiriliyor.
            Her bir inek, deve, koyun, keçi gibi mübarek hayvanlar Bismillah der. Rahmet feyzinden birer süt çeşmesi olur. Bizlere Rezzak namına en latîf, en nazif, âb-ı hayat gibi bir gıdayı takdim ediyorlar.
            Her bir nebat ve ağaç ve otların ipek gibi yumuşak kök ve damarları, Bismillah der. Sert olan taş ve toprağı deler, geçer. Allah namına, Rahman namına der, her şey ona musahhar olur. Evet, havada dalların intişarı ve meyve vermesi gibi o sert taş ve topraktaki köklerin kemal-i suhuletle intişar etmesi ve yer altında yemiş vermesi hem şiddet-i hararete karşı aylarca nazik, yeşil yaprakların yaş kalması, tabiiyyunun ağzına şiddetle tokat vuruyor. Kör olası gözüne parmağını sokuyor ve diyor ki:
            En güvendiğin salabet ve hararet dahi emir tahtında hareket ediyorlar ki, o ipek gibi yumuşak damarlar, birer asâ-yı Musa (as) gibi فَقُلْنَا اضْرِبْ بِعَصَاكَ الْحَجَرَ emrine imtisal ederek taşları şakkeder. Ve o sigara kâğıdı gibi ince nâzenin yapraklar, birer aza-yı İbrahim (as) gibi ateş saçan hararete karşı يَا نَارُ كُونٖى بَرْدًا وَ سَلَامًا âyetini okuyorlar.
            Madem her şey manen Bismillah der. Allah namına Allah’ın nimetlerini getirip bizlere veriyorlar. Biz dahi Bismillah demeliyiz. Allah namına vermeliyiz, Allah namına almalıyız. Öyle ise Allah namına vermeyen gafil insanlardan almamalıyız.
            Sual: Tablacı hükmünde olan insanlara bir fiyat veriyoruz. Acaba asıl mal sahibi olan Allah, ne fiyat istiyor?
            Elcevap: Evet, o Mün’im-i Hakiki, bizden o kıymettar nimetlere, mallara bedel istediği fiyat ise üç şeydir. Biri zikir, biri şükür, biri fikirdir.
            Başta Bismillah zikirdir.
            Âhirde Elhamdülillah şükürdür.
            Ortada, bu kıymettar hârika-i sanat olan nimetler Ehad-i Samed’in mu’cize-i kudreti ve hediye-i rahmeti olduğunu düşünmek ve derk etmek fikirdir. Bir padişahın kıymettar bir hediyesini sana getiren bir miskin adamın ayağını öpüp hediye sahibini tanımamak ne derece belâhet ise öyle de zahirî mün’imleri medih ve muhabbet edip Mün’im-i Hakiki’yi unutmak, ondan bin derece daha belâhettir.
            Ey nefis, böyle ebleh olmamak istersen Allah namına ver, Allah namına al, Allah namına başla, Allah namına işle. Vesselâm.` }
    
    }

    componentDidMount() {
        this.calculateWordWeight();
    }

    compare(a, b){
        if (a.count < b.count) return 1;
        if (b.count < a.count) return -1;

        return 0;
    }

    calculateWordWeight(){
        var value = " "+this.state.data + ""
        value = value.replace(/(\r\n|\n|\r)/gm, " ")
        value = value.replace(/\s+/g, " ");
        value = value.replace(/(\.|,|!|:|;)/g, " ")
        const regex = /([^ ]*(.) )/gi;

        console.log(value.toLowerCase())
        
        let words = value.toLowerCase().match(regex);
        let totalCount = words.length;
        words = [...new Set(words)];
        console.log(words)

        var wordItems = words.map(word => {
            var arr = value.split(" "+word);
            return {
                word,
                count: arr.length,
                weight: Number(((arr.length * 100) / totalCount).toFixed(2))
            }
        })

        wordItems = wordItems.sort(this.compare);

        this.setState({ wordItems });
    }


    render() {

        return (
            <div style={{display:'flex', flexDirection:'row',}}>
                 <div style={{height:'calc(100vh)',width:'100%', padding:10, overflow:'hidden'}}>
                     <div style={{fontWeight:'bold', marginBottom:10,}}>Text</div>
                <textarea
                style={{width:'95%', height:'calc(100vh - 64px)'}}
            multiple
                    value={this.state.data}
                    onChange={(e)=>{
                        this.setState({data:e.target.value});
                        setTimeout(() => {
                            this.calculateWordWeight();
                        }, 100);
                    }}
                />
                </div>

                <div style={{height:'calc(100vh)', overflow:'scroll', 
                marginLeft:16,
                 minWidth:400}}>
                 <div style={{
                    backgroundColor:"#FFFFFF", 
                borderRadius:4, 
                padding:10, 
                marginBottom:2,
                display:'flex',
                flexDirection:'row',
                position:'fixed',
                }}>
                    <div style={{width:200, maxWidth:200, fontWeight:'bold'}}>word</div>
                    <div style={{width:80, maxWidth:80, fontWeight:'bold'}}>count</div>
                    <div style={{width:80, maxWidth:80, fontWeight:'bold'}}>weight</div>
                </div>
                <div style={{height:42}}/>
                
                {this.state.wordItems.map((item, i) => 
                <div key={i} style={{
                    backgroundColor:"#FFFFFF", 
                borderRadius:4, 
                border:'1px solid #E0E0E0',
                padding:10, 
                marginBottom:2,
                display:'flex',
                flexDirection:'row',
                width:360
                }}>
                    <div style={{width:200, maxWidth:200}}>{item.word}</div>
                    <div style={{width:80, maxWidth:80}}>{item.count}</div>
                    <div style={{width:80, maxWidth:80}}>{"%"+item.weight}</div>
                </div>
                )}
                </div>
            </div>
        );
    }
}

export default App;
